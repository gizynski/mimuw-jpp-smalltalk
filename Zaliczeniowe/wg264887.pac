| package |
package := Package name: 'wg264887'.
package paxVersion: 1;
	basicComment: 'Autor: Wojciech Gizynski (wg264887), 
Kontakt: gizynski@gmail.com (mail na @students sie odbije, nie mam dostepu)

Komentarze do swojego rozwiazania umiescilem w zalaczonych klasach i ich metodach.
Swoje decyzje staram sie uzasadnic w miejscu ktore wydawalo sie najbardziej logiczne.

Chcialem podziekowac za to zadanie - duzo sie przy nim nauczylem i poczulem przyjemnosc
z pracy w Smalltalku. Bardzo ciekawe doswiadczenie.

Aha, poniewaz nie mialem mozliwosci pracowac na maszynach students, to sciagnalem Dolphina
w najnowszej wersji ze strony http://www.object-arts.com/ (czyli wersje 7.05). Na takiej tez
testowalem swoje rozwiazanie.

Uzycie pakietu: w Package Browser poprzez File > Install Package...'.


package classNames
	add: #C;
	add: #Clause;
	add: #Conjunction;
	add: #Constant;
	add: #Fact;
	add: #Pair;
	add: #PairValue;
	add: #Prolog;
	add: #Rule;
	add: #Term;
	add: #V;
	add: #Variable;
	yourself.

package globalNames
	add: #L;
	yourself.

package binaryGlobalNames: (Set new
	add: #L;
	yourself).

package globalAliases: (Set new
	yourself).

package setPrerequisites: (IdentitySet new
	add: '..\Core\Object Arts\Dolphin\Base\Dolphin';
	yourself).

package!

"Class Definitions"!

Object subclass: #C
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #Clause
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #Conjunction
	instanceVariableNames: 'car cdr'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #PairValue
	instanceVariableNames: 'car cdr'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #Prolog
	instanceVariableNames: 'database'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #Term
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #V
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Clause subclass: #Fact
	instanceVariableNames: 'atom'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Clause subclass: #Rule
	instanceVariableNames: 'head body'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Term subclass: #Constant
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Term subclass: #Pair
	instanceVariableNames: 'car cdr'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Term subclass: #Variable
	instanceVariableNames: 'assignment name version'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: 'variablesDictionary newestVersion'!

"Global Aliases"!


"Loose Methods"!

"End of package definition"!

"Source Globals"!

"Classes"!

C guid: (GUID fromString: '{6AD718D1-2458-4B4D-ADED-326E53BBDD9A}')!
C comment: 'Klasa C, podobnie jak V, jest fabryka udostepniajaca tylko jedna metode, klasowa. Za pomoca C tworzymy stale, czyli instancje klasy Constant.
Dwie stale utworzone z ta sama wartoscia, np. C % 1, C % 1.0 i C % 1 nie beda tym samym obiektem. Tworze osobne instancje, poniewaz nie pilnuje jakie stale powstaly do tej pory.
Natomiast, mysle ze warto tutaj wspomniec, ze przyjalem zalozenie iz stale nie beda zmienialy sie w czasie (w koncu sa stalymi).

To moze okazac sie istotne przy tworzeniu klauzul (Clause), gdzie tworzac fakt (Fact) badz regule (Rule) wykonuje swieza kopie calego obiektu, 
z dokladnoscia do podmiany zmiennych na nowe wersje i wlasnie zachowaniu tych samych referencji do stalych. Zmiana tego zachowania jest bardzo tania,
bo jedyne co nalezy zrobic to zmienic linijke w metodzie Constant>>#fresh:. Uznalem ze rozwiazanie niedublujace niepotrzebnie bytow bedzie lepsze i bardziej eleganckie.'!
!C categoriesForClass!Kernel-Objects! !
!C class methodsFor!

% aValueObject
	"Answer with new <Constant> object, with value set appropriately."

	^Constant value: aValueObject! !
!C class categoriesFor: #%!public! !

Clause guid: (GUID fromString: '{CBF5CEA5-B7E1-43DC-A294-BCE3EE0DFCE7}')!
Clause comment: '<Clause> jest abstrakcyjna nadklasa dla <Fact> i <Rule>, dwoch klas reprezentujacych Klauzule w programie.

Klasa glownie stworzona dla porzadku i dobrego designu. Obie metody sa do zaimplementowania w podklasach, a <Clause> nie ma zadnych zmiennych.
To podejscie ulatwia natomiast wykrycie tego ze jedna z podklas nie zaimplementowala metody, ktora byla jej odpowiedzialnoscia.'!
!Clause categoriesForClass!Kernel-Objects! !
!Clause methodsFor!

fresh: versionNumberForVariables
	"Answer with a fresh copy of the receiver, with every <Variable> included set to specified version."

	^self subclassResponsibility!

go: aTerm do: blockWithoutParameters with: prologThatSentTheMessage
	"Answer with receiver, regardless of effect - if block was executed or not."

	^self subclassResponsibility! !
!Clause categoriesFor: #fresh:!private! !
!Clause categoriesFor: #go:do:with:!public! !

Conjunction guid: (GUID fromString: '{9831626F-B0AC-4197-B15B-E2A80F957C0D}')!
Conjunction comment: 'Klasa reprezentuje Koniunkcje dwoch elementow, z ktorych oba to albo <Conjunction> albo <Term>.
Nazewnictwo car/cdr tlumacze w <Pair>, wiec tam odsylam po wiecej szczegolow.

<Conjunction> jako takie nie jest trakowane jak <Term>, nie mozna do niego wyslac tych samych metod (np. #@, #%).
Nie jest tez podklasa <Clause>. Jest za to wykorzystywana jako Zapytanie (nie ma takiej klasy z wyboru) albo tresc Reguly (Rule).'!
!Conjunction categoriesForClass!Kernel-Objects! !
!Conjunction methodsFor!

& anotherTerm
	"Answer with a <Conjunction> object, by putting receiver as the first element and another <Term> as the second."

	^Conjunction car: self cdr: anotherTerm!

car
	"Answer with first element value."

	^car!

car: firstElement cdr: secondElement
	"Answer with receiver. Private method used for <Conjunction> creation and not meant to be used in other context."

	car := firstElement.
	cdr := secondElement!

cdr
	"Answer with second element."

	^cdr!

fresh: versionNumberForVariables
	"Answer with a fresh copy of the receiver, with every <Variable> included set to specified version."

	^Conjunction car: (car fresh: versionNumberForVariables) cdr: (cdr fresh: versionNumberForVariables)!

printOn: target
	"Answer with receiver, print object for easier development. For more verbose version use commented out approach."

	target nextPutAll: car printString , ' & ' , cdr printString
	" target nextPutAll: self class name , '(' , car printString , ' & ' , cdr printString , ')' "! !
!Conjunction categoriesFor: #&!public! !
!Conjunction categoriesFor: #car!public! !
!Conjunction categoriesFor: #car:cdr:!private! !
!Conjunction categoriesFor: #cdr!public! !
!Conjunction categoriesFor: #fresh:!public! !
!Conjunction categoriesFor: #printOn:!public! !

!Conjunction class methodsFor!

car: firstElement cdr: secondElement
	"Answer with <Conjunction> object, with first and second elements set accordingly to arguments. Used by #& in <Conjunction> and <Term> objects."

	^super new car: firstElement cdr: secondElement! !
!Conjunction class categoriesFor: #car:cdr:!public! !

PairValue guid: (GUID fromString: '{F1283D5D-1469-47CC-AF33-48B55027EC2C}')!
PairValue comment: '<PairValue> jest uzywany do przechowywania wartosci <Pair> po obluzeniu komunikatu #value. Uwazam ze takie rozwiazanie jest czystsze 
(niz uzycie do tego <Pair>), szczegolnie w obliczu tego jak Smalltalk jest otwarty na uzytkownika. Nie chcemy przeciez zeby metody #includes: albo #go:do: byly
wykonywane na obiekcie z policzonymi wartosciami Pary. <PairValue> budowany jest z polaczenia odpowiedzi na #value dla obu elementow Pary.'!
!PairValue categoriesForClass!Kernel-Objects! !
!PairValue methodsFor!

= anotherPair
	"Answer with true/false if receiver is equal to another <PairValue>, which happens if both elements equal that of another <PairValue>."

	anotherPair isKindOf: self class.
	^self car = anotherPair car & (self cdr = anotherPair cdr)!

car
	"Answer with first element."

	^car!

car: firstElement cdr: secondElement
	"Answer with receiver. Private method used for <PairValue> creation and not meant to be used in other context."

	car := firstElement.
	cdr := secondElement!

cdr
	"Answer with second element."

	^cdr!

hash
	"Private - Answer with hash value, constructed from hashes of both elements. Required, since I decided to override equality #= method.
	That was done to be able to use <PairValue> for keys in the dictionary controlling variables. See <Variable> class for more."

	^self car hash + self cdr hash!

printOn: target
	"Answer with receiver, print object for easier development. For more verbose version use commented out approach."

	target nextPutAll: '(' , car printString , ' , ' , cdr printString , ')'
	" target nextPutAll: self class name , '(' , car printString , ' , ' , cdr printString , ')' "! !
!PairValue categoriesFor: #=!public! !
!PairValue categoriesFor: #car!public! !
!PairValue categoriesFor: #car:cdr:!private! !
!PairValue categoriesFor: #cdr!public! !
!PairValue categoriesFor: #hash!private! !
!PairValue categoriesFor: #printOn:!public! !

!PairValue class methodsFor!

car: firstElement cdr: secondElement
	"Answer with <PairValue> object, with first and second elements set accordingly to arguments."

	^super new car: firstElement cdr: secondElement! !
!PairValue class categoriesFor: #car:cdr:!public! !

Prolog guid: (GUID fromString: '{C8595B6C-F61B-4AE8-B19F-71D58F971E41}')!
Prolog comment: 'Klasa reprezentuje Interpretator Prologu. W ogromnej mierze opiera sie na zaimplementowanym protokole go:do: z pierwszej czesci zadania.
W sytuacji odbioru zapytania (#go:do:), przegladana jest cala baza danych w poszukiwaniu wszystkich drog do udowodnienia prawdziwosci zapytania.
Dla kazdej z tych sciezek wykonywany jest blok do:. Przy przegladaniu bazy danych kazda klauzula jest odswiezana z najnowszym numerem wersji dla zmiennych.

Baza danych jest inicjalizowana przy utworzeniu nowej instancji klasy <Prolog>.'!
!Prolog categoriesForClass!Kernel-Objects! !
!Prolog methodsFor!

fact: aTerm
	"Answer with receiver. Add to database new instance of <Fact> class, use supplied value for object creation."

	database addLast: (Fact atom: (aTerm fresh: Variable newVersionNumber))!

go: aQuestion do: blockWithoutParameters
	"Answer with receiver. If possible, find unification that allows to prove aQuestion based on knowledge in the databse. If successful, execute supplied block.
	In case of multiple ways of proving aQuestion, go through all of them, executing block each time. Use DFS approach. Note that it's assumed that if aQuestion
	is not a single <Term>, it must be a <Conjunction> which is then broken into smaller bits, Terms and processed one by one. Divide and conquer."
	
	(aQuestion isKindOf: Term)
		ifFalse: [^self go: aQuestion car do: [self go: aQuestion cdr do: blockWithoutParameters]].
	database do: 
			[:clause |
			| freshClause versionNumber |
			versionNumber := Variable newVersionNumber.
			freshClause := clause fresh: versionNumber.
			freshClause
				go: aQuestion
				do: blockWithoutParameters
				with: self.
			Variable forgetVersion: versionNumber]!

head: aTerm body: aConjunctionOrSingleTerm
	"Answer with receiver. Add to database new instance of <Rule> class, use supplied values for object creation."

	| newVersion |
	newVersion := Variable newVersionNumber.
	database
		addLast: (Rule head: (aTerm fresh: newVersion) body: (aConjunctionOrSingleTerm fresh: newVersion))!

initialize
	"Answer with receiver, set database variable value to it's initial state."

	database := OrderedCollection new!

printOn: target
	"Answer with receiver, print object for easier development."

	| databaseSize numberOfFacts numberOfRules |
	databaseSize := database size.
	numberOfFacts := (database select: [:info | info isKindOf: Fact]) size.
	numberOfRules := (database select: [:info | info isKindOf: Rule]) size.
	target
		nextPutAll: self class name , '(DB: ' , databaseSize printString , ', Facts: '
				, numberOfFacts printString , ', Rules: '
				, numberOfRules printString , ')'! !
!Prolog categoriesFor: #fact:!public! !
!Prolog categoriesFor: #go:do:!public! !
!Prolog categoriesFor: #head:body:!public! !
!Prolog categoriesFor: #initialize!private! !
!Prolog categoriesFor: #printOn:!public! !

!Prolog class methodsFor!

new
	"Answer with new instance of <Prolog>, with empty database."

	^super new initialize! !
!Prolog class categoriesFor: #new!public! !

Term guid: (GUID fromString: '{FE3687F9-9AE2-47B1-ABE5-BB73FD4BA977}')!
Term comment: 'Abstrakcyjna klasa, ktorej podklasy realizuja stala (Constant), zmienna (Variable) i pare termow (Pair). 

Zaimplementowanie metod #value, #fresh: i #includes: lezy po stronie podklas. Kod wspolny dla metody #unify:temp: znalazl sie w tej klasie,
choc po stronie podklas lezy odpowiedzialnosc za stosowne uzupelnienie tej metody.

Wspolne czesci, ktore mogly byc w calosci zaimplementowane w Term to: #%, #@, #&, #, oraz #go:do:.

Klasa Term jest wykorzystywana jako atom w tej implementacji Prologu. Polaczenie dwoch Termow za pomoca #& sprawia ze '!
!Term categoriesForClass!Kernel-Objects! !
!Term methodsFor!

% aValueForConstant
	"Answer with a <Pair> object, by putting receiver as the first element and new <Constant> as the second."

	^Pair car: self cdr: C % aValueForConstant!

& anotherTerm
	"Answer with a <Conjunction> object, by putting receiver as the first element and another <Term> as the second."

	^Conjunction car: self cdr: anotherTerm!

, anotherTerm
	"Answer with a <Pair> object, by putting receiver as the first element and another <Term> as the second."

	^Pair car: self cdr: anotherTerm!

@ variableName
	"Answer with a <Pair> object, by putting receiver as the first element and new or existing <Variable> as the second."

	^Pair car: self cdr: V @ variableName!

fresh: versionNumberForVariables
	"Answer with a fresh copy of the receiver, with every <Variable> included set to specified version."

	^self subclassResponsibility!

go: anotherTerm do: blockWithoutParameters
	"Answer with receiver, regardless of effect - if block was executed or not."

	| temporaryAssignments |
	temporaryAssignments := Bag new.
	(self unify: anotherTerm temp: temporaryAssignments) ifTrue: [blockWithoutParameters value].
	temporaryAssignments do: [:variable | variable unassign]!

includes: aVariable
	"Answer true/false if the receiver <Term> includes in it's structure specific <Variable>. That's known as the 'check' step from unification algorithm."

	^self subclassResponsibility!

unify: anotherTerm temp: temporaryAssignments
	"Answer with true/false whether the receiver unifies with another <Term>.  Second argument is specified for tracking possible assignments of variables."

	^self == anotherTerm!

value
	"Answer with a value of receiver."

	^self subclassResponsibility! !
!Term categoriesFor: #%!public! !
!Term categoriesFor: #&!public! !
!Term categoriesFor: #,!public! !
!Term categoriesFor: #@!public! !
!Term categoriesFor: #fresh:!public! !
!Term categoriesFor: #go:do:!public! !
!Term categoriesFor: #includes:!public! !
!Term categoriesFor: #unify:temp:!public! !
!Term categoriesFor: #value!public! !

V guid: (GUID fromString: '{9F930C8A-FB60-40C1-A718-AA8DFBFC63A5}')!
V comment: 'Klasa V to fabryka zmiennych. Dostepna jest tylko jedna metoda klasowa #@, ktora zwraca referencje do obiektu Variable.
Mozna bylo w tym miejscu (klasa V) umiescic kontrole nad zmiennymi, poprzez slownik itd.
Zdecydowalem sie na trzymanie tych informacji w klasie Variable, jako ze to zmiennych one wlasnie dotycza.
Uznalem takie rozwiazanie za czystsze - fabryka V sluzy do uzyskiwania zmiennych, ktore maja byc jednoznacznie identyfikowane nazwa,
natomiast jest to uzyskiwane przez delegowanie zadania do klasy Variable, ktora potrafi tworzyc zmienne nie stosujac sie do tego wymagania.

W swoim rozwiazaniu zapamietuje tez "wersje" w ktorej zostala stworzona zmienna, w zaleznosci od kontekstu. Wiecej informacji w klasie Variable.

Fragment tresci zadania: "Produkowane przez V zmienne sa jednoznacznie identyfikowane nazwa".'!
!V categoriesForClass!Kernel-Objects! !
!V class methodsFor!

@ variableName
	"Answer with a <Variable> object - either new or existing (with requested name) in general context."

	^Variable name: variableName version: 0! !
!V class categoriesFor: #@!public! !

Fact guid: (GUID fromString: '{F5B3BA87-C6FC-4D98-A035-4F0F61D2D5A0}')!
Fact comment: 'Klasa reprezentuje Fakt, ktory mozna spotkac w bazie danych obiektu <Prolog>.
Atomem moze byc jedynie <Term>, nie moze nim byc <Conjunction>.'!
!Fact categoriesForClass!Kernel-Objects! !
!Fact methodsFor!

atom: aTerm
	"Answer with receiver. Private method used for instance creation, not meant to be used in any other context."

	atom := aTerm!

fresh: versionNumberForVariables
	"Answer with a fresh copy of the receiver, with every <Variable> included set to specified version."

	^Fact atom: (atom fresh: versionNumberForVariables)!

go: aTerm do: blockWithoutParameters with: prologThatSentTheMessage
	"Answer with answer from atom to #go:do: message. Last argument is ignored."

	^atom go: aTerm do: blockWithoutParameters!

printOn: target
	"Answer with receiver, print object for easier development."

	target nextPutAll: self class name , '(' , atom printString , ')'! !
!Fact categoriesFor: #atom:!private! !
!Fact categoriesFor: #fresh:!private! !
!Fact categoriesFor: #go:do:with:!public! !
!Fact categoriesFor: #printOn:!public! !

!Fact class methodsFor!

atom: aTerm
	"Answer with new instance of <Fact> with specified term as an atom."

	^super new atom: aTerm! !
!Fact class categoriesFor: #atom:!public! !

Rule guid: (GUID fromString: '{64014B31-51C3-4B5A-B6C3-0A76C3C5BCA8}')!
Rule comment: 'Klasa reprezentujaca Regule wnioskowania, ktora mozna znalezc w bazie danych obiektu <Prolog>.
Wartoscia head moze byc jedynie <Term>, nie moze nim byc <Conjunction>.'!
!Rule categoriesForClass!Kernel-Objects! !
!Rule methodsFor!

fresh: versionNumberForVariables
	"Answer with a fresh copy of the receiver, with every <Variable> included set to specified version."

	^Rule head: (head fresh: versionNumberForVariables) body: (body fresh: versionNumberForVariables)!

go: aTerm do: blockWithoutParameters with: prologThatSentTheMessage
	"Answer with answer from head to #go:do: message that adds body part of <Rule> to unification.
	If that fails, the entire attempt to unify with <Rule> fails and final block is never reached."

	^head go: aTerm do: [prologThatSentTheMessage go: body do: blockWithoutParameters]!

head: aTerm body: conjuctionOrSingleTerm
	"Answer with receiver. Private method used for instance creation, not meant to be used in any other context."

	head := aTerm.
	body := conjuctionOrSingleTerm!

printOn: target
	"Answer with receiver, print object for easier development. For more verbose version use commented out approach."

	target nextPutAll: '(' , head printString , ' <- ' , body printString , ')'
	" target nextPutAll: self class name , '(' , head printString , ' <- ', body printString ,  ')' "! !
!Rule categoriesFor: #fresh:!public! !
!Rule categoriesFor: #go:do:with:!public! !
!Rule categoriesFor: #head:body:!public! !
!Rule categoriesFor: #printOn:!public! !

!Rule class methodsFor!

head: aTerm body: conjuctionOrSingleTerm
	"Answer with new instance of <Rule> with specified <Term> as head and <Term> or <Conjunction> as body."

	^super new head: aTerm body: conjuctionOrSingleTerm! !
!Rule class categoriesFor: #head:body:!public! !

Constant guid: (GUID fromString: '{22E7CED3-D265-46A5-9402-0965ACDAFE08}')!
Constant comment: 'Klasa ktora reprezentuje stale w programie. 

Dwie stale utworzone z ta sama wartoscia, np. 1, 1.0 i 1 nie beda tym samym obiektem. Tworze osobne instancje, poniewaz nie pilnuje jakie stale powstaly do tej pory.
Natomiast, mysle ze warto tutaj wspomniec, ze przyjalem zalozenie iz stale nie beda zmienialy sie w czasie (w koncu sa stalymi).

To moze okazac sie istotne przy tworzeniu klauzul (Clause), gdzie tworzac fakt (Fact) badz regule (Rule) wykonuje swieza kopie calego obiektu, 
z dokladnoscia do podmiany zmiennych na nowe wersje i wlasnie zachowaniu tych samych referencji do stalych. Zmiana tego zachowania jest bardzo tania,
bo jedyne co nalezy zrobic to zmienic linijke w metodzie Constant>>#fresh:. Uznalem ze rozwiazanie niedublujace niepotrzebnie bytow bedzie lepsze i bardziej eleganckie.'!
!Constant categoriesForClass!Kernel-Objects! !
!Constant methodsFor!

fresh: versionNumberForVariables
	"Answer with a fresh copy of the receiver, with every <Variable> included set to specified version."

	^self!

includes: aVariable
	"Answer true/false if the receiver <Term> includes in it's structure specific <Variable>. That's known as the 'check' step from unification algorithm.
	Constant obviously can't ever include a <Variable> in a sense of having it in it's structure."

	^false!

printOn: target
	"Answer with receiver, print object for easier development. For more verbose version use commented out approach."

	target nextPutAll: '%' , value printString
	" target nextPutAll: self class name , '(' , value printString , ')' "!

unify: anotherTerm temp: temporaryAssignments
	"Answer with true/false whether the receiver unifies with another <Term>.  Second argument is specified for tracking possible assignments of variables."

	(super unify: anotherTerm temp: temporaryAssignments) ifTrue: [^true].
	(anotherTerm isKindOf: Constant) ifTrue: [^value = anotherTerm value].
	(anotherTerm isKindOf: Variable) ifTrue: [^anotherTerm unify: self temp: temporaryAssignments].
	^false!

value
	"Answer with value of variable value."

	^value!

value: anObject
	"Answer with receiver. Private method used for instance creation, not meant to be used in any other context."

	value := anObject! !
!Constant categoriesFor: #fresh:!public! !
!Constant categoriesFor: #includes:!public! !
!Constant categoriesFor: #printOn:!public! !
!Constant categoriesFor: #unify:temp:!public! !
!Constant categoriesFor: #value!public! !
!Constant categoriesFor: #value:!private! !

!Constant class methodsFor!

value: anObject
	"Answer with new instance of <Constant> with specified constant value."

	^super new value: anObject! !
!Constant class categoriesFor: #value:!public! !

Pair guid: (GUID fromString: '{3B08C915-DABF-4FB5-9944-AF12945F3915}')!
Pair comment: 'Klasa <Pair> reprezentuje Termy w postaci pary dwoch Termow. Oba elementy mozna uzyskac przez odpowiednie gettery.
Zdecydowalem sie nazwac zmienne tej klasy car/cdr zamiast first/second albo x/y czy a/b, poniewaz konwencja Smalltalkowa
jest nazywanie metod dostepowych (getterow) tak samo jak zmiennych obiektu. Poniewaz #car i #cdr byly narzucone z gory,
zastosowalem takie podejscie w calym projekcie.

Warto wspomniec jeszcze ze przy obsludze komunikatu #value zwracany jest obiekt <PairValue>, nie zwykla Para. Uwazam ze takie rozwiazanie
jest czystsze, szczegolnie w obliczu tego jak Smalltalk jest otwarty na uzytkownika. Nie chcemy przeciez zeby metody #includes: albo #go:do: byly
wykonywane na obiekcie z policzonymi wartosciami Pary. <PairValue> budowany jest z polaczenia odpowiedzi na #value dla obu elementow Pary.'!
!Pair categoriesForClass!Kernel-Objects! !
!Pair methodsFor!

car
	"Answer with first element value."

	^car!

car: firstElement cdr: secondElement
	"Answer with receiver. Private method used for <Pair> creation and not meant to be used in other context."

	car := firstElement.
	cdr := secondElement!

cdr
	"Answer with second element value."

	^cdr!

fresh: versionNumberForVariables
	"Answer with a fresh copy of the receiver, with every <Variable> included set to specified version."

	^Pair car: (car fresh: versionNumberForVariables) cdr: (cdr fresh: versionNumberForVariables)!

includes: aVariable
	"Answer true/false if the receiver <Term> includes in it's structure specific <Variable>. That's known as the 'check' step from unification algorithm."

	^(car includes: aVariable) | (cdr includes: aVariable)!

printOn: target
	"Answer with receiver, print object for easier development. For more verbose version use commented out approach."

	target nextPutAll: '(' , car printString , ' , ' , cdr printString , ')'
	" target nextPutAll: self class name , '(' , car printString , ' , ' , cdr printString , ')' "!

unify: anotherTerm temp: temporaryAssignments
	"Answer with true/false whether the receiver unifies with another <Term>.  Second argument is specified for tracking possible assignments of variables."

	(super unify: anotherTerm temp: temporaryAssignments) ifTrue: [^true].
	(anotherTerm isKindOf: Pair)
		ifTrue: 
			[^(car unify: anotherTerm car temp: temporaryAssignments)
				& (cdr unify: anotherTerm cdr temp: temporaryAssignments)].
	(anotherTerm isKindOf: Variable) ifTrue: [^anotherTerm unify: self temp: temporaryAssignments].
	^false!

value
	"Answer with a <PairValue> object used to represent value of receiver."

	^PairValue car: car value cdr: cdr value! !
!Pair categoriesFor: #car!public! !
!Pair categoriesFor: #car:cdr:!private! !
!Pair categoriesFor: #cdr!public! !
!Pair categoriesFor: #fresh:!public! !
!Pair categoriesFor: #includes:!public! !
!Pair categoriesFor: #printOn:!public! !
!Pair categoriesFor: #unify:temp:!public! !
!Pair categoriesFor: #value!public! !

!Pair class methodsFor!

car: firstElement cdr: secondElement
	"Answer with <Pair> object, with first and second elements set accordingly to arguments."

	^super new car: firstElement cdr: secondElement! !
!Pair class categoriesFor: #car:cdr:!public! !

Variable guid: (GUID fromString: '{97DD6788-E4D1-4502-8252-C1456BE7E8D4}')!
Variable comment: 'Klasa Variable zajmuje sie pelna kontrola nad zmiennymi. Tutaj odbywa sie ich faktyczne tworzenie i uzyskiwanie juz istniejacych o zadanych parametrach.
Oprocz nazwy przechowuje w slowniku zmiennych tez informacje o tym w jakiej wersji (wiec poniekad - kontekscie) zostala utworzona. Jesli wersja jest wyzsza od 0,
oznacza to ze zmienna byla tworzona albo na potrzeby klauzuli (Clause) przed umieszczeniem w bazie danych Prologa albo byla uzyta przy swiezej kopii jakiegos obiektu 
(Termu, Koniunkcji lub Klauzuli). Gdy dana wersja przestaje byc juz potrzebna, jest zapominana przy pomocy metody #forgetVersion:. Robiac inspekcje slownika, 
po wykonaniu testow, mozna zauwazyc ze nie wszystkie zmienne sa w wersji 0. Wynika to z tego, ze informacje umieszczane w bazach danych roznych instancji Prolog
nadal istnieja w tych bazach danych. Jesli nieuzywanym Prologiem zajal sie Garbage Collector, to zmienne nadal pozostana w slowniku - ich usuniecie jest nietrywialne,
a koszt utrzymania minimalny (nigdy nie sa przypisane w tych wersjach, wiec nie podtrzymuja zadnego obiektu przy zyciu).

Metody #car i #cdr znajduja sie w klasie ze wzgledu na potencjalne ustalenie zmiennej z para (Pair). Mozna bylo to obsluzyc tez tak ze dowolny komunikat ktory nie jest zrozumialy
dla zmiennej, skutkuje bledem (messageNotUnderstood) w przypadku nieustalonego stanu, badz komunikat jest przerzucany do ustalenia zmiennej i tam obslugiwany (potencjalnie z tym samym rezultatem).'!
!Variable categoriesForClass!Kernel-Objects! !
!Variable methodsFor!

car
	"Answer with first element of the assignment, or <Error> in case this is impossible."

	self isAssigned ifFalse: [^self error: 'Can''t get car of unassigned Variable ' , name printString].
	(assignment isKindOf: Constant)
		ifTrue: [^self error: 'Can''t get car of Variable ' , name printString , ' assigned to a Constant'].
	^assignment car!

cdr
	"Answer with second element of the assignment, or <Error> in case this is impossible."
	
	self isAssigned ifFalse: [^self error: 'Can''t get cdr of unassigned Variable ' , name printString].
	(assignment isKindOf: Constant)
		ifTrue: [^self error: 'Can''t get cdr of Variable ' , name printString , ' assigned to a Constant'].
	^assignment cdr!

fresh: versionNumberForVariables
	"Answer with a fresh copy of the <Variable>, meaning the same name, but a different, requested version."

	^Variable name: name version: versionNumberForVariables!

includes: aVariable
	"Answer true/false if the receiver <Term> includes in it's structure specific <Variable>. That's known as the 'check' step from unification algorithm."

	self == aVariable ifTrue: [^true].
	self isAssigned ifTrue: [^assignment includes: aVariable].
	^false!

isAssigned
	"Answer whether the <Variable> is set (assigned) at the moment."

	^assignment isNil not!

name: aVariableName version: aPositiveInteger
	"Answer with receiver. Set variable values accordingly."

	name := aVariableName.
	version := aPositiveInteger!

printOn: target
	"Answer with receiver, print object for easier development. For more verbose version use commented out approach."

	target nextPutAll: '@' , name printString
	" target nextPutAll: self class name , '(' , name printString , ')' "!

unassign
	"Answer with receiver. Mark <Variable> as not set to anything."

	assignment := nil!

unify: anotherTerm temp: temporaryAssignments
	"Answer with true/false whether the receiver unifies with another <Term>.  In case of success, set the assignment and remember this in collection from temp:."

	(super unify: anotherTerm temp: temporaryAssignments) ifTrue: [^true].
	self isAssigned ifTrue: [^assignment unify: anotherTerm temp: temporaryAssignments].
	(anotherTerm isKindOf: Variable)
		ifTrue: [anotherTerm isAssigned ifTrue: [^anotherTerm unify: self temp: temporaryAssignments]].
	(anotherTerm includes: self) ifTrue: [^false].
	assignment := anotherTerm.
	temporaryAssignments add: self.
	^true!

value
	"Answer with a value of Variable's assignment or <Error> if it wasn't set."

	self isAssigned ifTrue: [^assignment value].
	^self error: 'Can''t get value of unassigned Variable ' , name printString! !
!Variable categoriesFor: #car!public! !
!Variable categoriesFor: #cdr!public! !
!Variable categoriesFor: #fresh:!public! !
!Variable categoriesFor: #includes:!public! !
!Variable categoriesFor: #isAssigned!private! !
!Variable categoriesFor: #name:version:!private! !
!Variable categoriesFor: #printOn:!public! !
!Variable categoriesFor: #unassign!public! !
!Variable categoriesFor: #unify:temp:!public! !
!Variable categoriesFor: #value!public! !

!Variable class methodsFor!

forgetVersion: versionToForget
	"Answer with receiver. Remove from the control dictionary all variables in version passed as an argument."

	(variablesDictionary keys select: [:oldKey | oldKey cdr = versionToForget])
		do: [:oldKey | variablesDictionary removeKey: oldKey]!

initialize
	"Answer with receiver. Set class variables to initial values."

	super initialize.
	newestVersion := 0.
	variablesDictionary := Dictionary new!

name: aVariableName version: aPositiveInteger
	"Answer with <Variable> object, either new or existing for specific name and version. Note that <V> only requests variables in version 0 (general).
	Use <PairValue> object as the key in the variablesDictionary, to track both name and version of variables stored there."

	| variableSelector |
	variableSelector := PairValue car: aVariableName cdr: aPositiveInteger.
	^variablesDictionary at: variableSelector
		ifAbsent: 
			[variablesDictionary at: variableSelector
				put: (super new name: aVariableName version: aPositiveInteger)]!

newVersionNumber
	"Answer with new, incremented value that's safe for use in 'fresh' copies of Variables."

	^newestVersion := newestVersion + 1! !
!Variable class categoriesFor: #forgetVersion:!public! !
!Variable class categoriesFor: #initialize!public! !
!Variable class categoriesFor: #name:version:!public! !
!Variable class categoriesFor: #newVersionNumber!public! !

"Binary Globals"!

L := Object fromBinaryStoreBytes: 
(ByteArray fromBase64String: 'IVNUQiAzIAYBCABDb25zdGFudAAAAAA=')!

