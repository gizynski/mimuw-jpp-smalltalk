" Testy z tresci zadania "

t := L % 1, (V @ 2 % 3).
self assert: [t car car value isNil].
self assert: [t car cdr value = 1].
self assert: [t car value car isNil].
self assert: [t car value cdr = 1].
self assert: [t cdr cdr value = 3].

a := V @ 1.
b := V @ 2.
c := V @ 1.
self assert: [a ~= b].
self assert: [a == c].

t := C % 1 @ #z.
self assert: [t car value = 1].
w := 0.
[t value] on: Error do: [:ex | w := w + 1].
self assert: [w = 1].

C % 1 go: C % 2 do: [self assert: [false]].

w := 0.
C % 1 go: C % 1 do: [w := w + 1].
self assert: [w = 1].

x := V @ #x.
y := V @ #y.
w := 0.
x % 1 go: C % 2, y do:
    [w := w + 1.
    self assert: [x value = 2].
    self assert: [y value = 1]].
self assert: [w = 1].

w := 0.
C % 1 % 2 go: x do:
    [w := w + 1.
    self assert: [x value car = 1].
    self assert: [x cdr value = 2]].
self assert: [w = 1].

w := 0.
x go: C % $a do:
    [w := w + 1.
    x go: C % $b do: [self assert: [false]]].
x go: C % $b do: [w := w + 1].
self assert: [w = 2].

w := 0.
x % 1 go: y, y do:
    [w := w + 1.
    self assert: [x value = 1].
    self assert: [y value = 1]].
self assert: [w = 1].

x go: L, x do: [self assert: [false]].

" Moje testy "

"nieudana unifikacja, x->y, y->Para z x"
w := 0.
x , y go: y , (x % 1) do:
    [w := w + 1].
self assert: [w = 0].

"przyklad p. Chrzaszcza nr 1"
w := 0.
x , y go: y , y do:
    [w := w + 1.
	y go: (C % 1) do: [self assert: [(y value = 1) | (x value = 1)]]
].
self assert: [w = 1].

"przyklad p. Chrzaszcza nr 2"
w := 0.
x , y go: y , y do:
    [w := w + 1.
	(C % 1) go: x do: [self assert: [(y value = 1) | (x value = 1)]]
].
self assert: [w = 1].

"problematyczne przypisywanie na sama siebie - petla, symetrycznosc uzgadniania"

w := 0.
x go: y do: [x go: x do: [w := 1]].
self assert: [w = 1].

w := 0.
x go: y do: [x go: y do: [w := 1]].
self assert: [w = 1].

w := 0.
x go: y do: [y go: x do: [w := 1]].
self assert: [w = 1].




"(C % 1) go: (C % 1) do: [true]
(C % 1) go: (C % 2) do: [true]
 (C % 1) go: (V @ #a) do: [(V @ #a) go: (C % 1) do: [Transcript show: 'hello']]"

"
(PairValue car: 5 cdr: 0) = (PairValue car: 5 cdr: 0)
(PairValue car: 5 cdr: 0) hash

Variable

PairValue hash"

"Fact atom: (C % 1)
Variable
(V @ #deepCopy) unify: (C % 1) temp: Bag new
v2 := (V @ #deepCopy) copy
v1 := (V @ #deepCopy) 

t1 := (C % 1) @ 2 @ 3 % 4 % 5 @ 6 % 7 @ 8 % 9
t2 := t1 fresh: 1
t1 == t2

oc := OrderedCollection new.

p := Prolog new.
p fact: (V @ 1)
v2 := (V @ 2).
p go: v2 do: [Transcript show ; show: 'yay ' , v2 value printString ; cr].
f1 := Fact atom: (C %1)
f1 go: (C % 1) do: [Transcript show ; show: 'yay' ; cr]
p go: (v2 & (C % 7)) do: [Transcript show ; show: 'yay ' , v2 value printString ; cr]

q1 := (v2 & (C % 7)) & (C % 5)

p go: q1 do: [Transcript show ; show: 'yay ' , v2 value printString ; cr]

p2 := Prolog new.
(V @ 5) isKindOf: Term"

" Przyklad drugi z tresci zadania "

x := V @ #x.
t := #(1 2 3) asOrderedCollection.
p := Prolog new.
t do: [:each | p fact: C % each].
w := OrderedCollection new.
p go: x do: [w add: x value].
self assert: [w = t].
w := 0.
p go: (C % 1) & (C % 2) & (C % 3) do: [w := w + 1].
self assert: [w = 1].
w := 0.
p go: (C % 1) & ((C % 2) & (C % 3)) do: [w := w + 1].
self assert: [w = 1].

y := V @ #y.
p := Prolog new
    fact: C % 1 % $a;
    fact: C % 2 % $b;
    head: x, y % $c body: (x % $a) & (y % $b);
    yourself.
w := 0.
p go: x, y % $c do:
    [w := w + 1.
    self assert: [x value = 1].
    self assert: [y value = 2]].
self assert: [w = 1].

z := V @ #z.
p := Prolog new
    fact: x, (y, x) % #member;
    head: x, (y, z) % #member body: x, y % #member;
    yourself.
w := OrderedCollection new.
p go: x, (L % 1 % 2 % 3) % #member do: [w add: x value].
self assert: [w = #(3 2 1) asOrderedCollection].

w := OrderedCollection new.
m := L % 1 % 2 % 3 % 4.
n := L % 0 % 2 % 4 % 6.
p go: (x, m % #member) & (x, n % #member) do: [w add: x value].
self assert: [w = #(4 2) asOrderedCollection].

a := V @ #a.
b := V @ #b.
c := V @ #c.
p fact: L, x, x % #append.
p head: (a, x), b, (c, x) % #append body: a, b, c % #append.
w := OrderedCollection new.
p go: x, y, (L % $c % $b % $a) % #append do:
    [[:q |
    s := OrderedCollection new.
    p go: a, q % #member do: [s add: a value].
    w add: (String withAll: s)]
        value: x;
        value: y].
self assert: [w = #('' 'abc' 'a' 'bc' 'ab' 'c' 'abc' '') asOrderedCollection]
