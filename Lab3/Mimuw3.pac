| package |
package := Package name: 'Mimuw3'.
package paxVersion: 1;
	basicComment: ''.


package classNames
	add: #Gracz;
	add: #Karta;
	add: #Rozdanie;
	yourself.

package binaryGlobalNames: (Set new
	yourself).

package globalAliases: (Set new
	yourself).

package setPrerequisites: (IdentitySet new
	add: 'Core\Object Arts\Dolphin\Base\Dolphin';
	add: 'Core\Object Arts\Dolphin\Base\Dolphin Additional Sort Algorithms';
	yourself).

package!

"Class Definitions"!

Object subclass: #Gracz
	instanceVariableNames: 'name cards'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #Karta
	instanceVariableNames: 'value color'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #Rozdanie
	instanceVariableNames: 'players'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: 'deckSingleton'!

"Global Aliases"!


"Loose Methods"!

"End of package definition"!

"Source Globals"!

"Classes"!

Gracz guid: (GUID fromString: '{E7085812-B76D-43EE-A6C7-2E6408B5DB1D}')!
Gracz comment: 'Sortowanie kart na rece rozwiazalem za pomoca sortUsing z sortAlgorithmem, typu Pluggable, do ktorego mozna podlaczyc wlasny sortBlock.
Nie wiem czy taki byl zamysl autora zadania. Pewnie innym (mozliwe ze prostszym) sposobem bylo uzycie SortableCollection i tam przekazanie sortBlock.

W kazdym razie, ja definicje algorytmu sortujacego trzymam w metodzie klasowej Karty - to wydawalo sie sensowne, bo to karty powinny wiedziec jak je porownywac.'!
!Gracz categoriesForClass!Kernel-Objects! !
!Gracz methodsFor!

displayOn: aStream
	aStream
		nextPutAll: '(' , name , ')';
		cr.
	cards sortUsing: Karta displaySortAlgorithm.
	(4 to: 1 by: -1) do: 
			[:c |
			| cardsInColor |
			aStream nextPutAll: (Karta displayColor: c) , '    '.
			cardsInColor := cards select: [:card | card color = c].
			cardsInColor size > 0
				ifTrue: 
					[cardsInColor allButLast do: 
							[:card |
							card displayOn: aStream.
							aStream nextPutAll: ' '].
					cardsInColor last displayOn: aStream].
			aStream cr]!

name: n cards: c
	name := n.
	cards := c!

printOn: target
	target nextPutAll: name , '('.
	cards sortUsing: Karta printSortAlgorithm.
	cards allButLast do: [:c | target nextPutAll: c printString , ', '].
	target nextPutAll: cards last printString , ')'! !
!Gracz categoriesFor: #displayOn:!public! !
!Gracz categoriesFor: #name:cards:!public! !
!Gracz categoriesFor: #printOn:!public! !

!Gracz class methodsFor!

name: n cards: c
	^super new name: n cards: c! !
!Gracz class categoriesFor: #name:cards:!public! !

Karta guid: (GUID fromString: '{C6499701-41F1-4260-8C53-791993C637F1}')!
Karta comment: 'Mam problem jesli chodzi o podjecie decyzji jak to zaimplementowac.
Wyobrazam sobie kilka wariantow, ale nie jestem w stanie powiedziec ktory z nich bedzie lepszy, bardziej odpowiedni do zadania:
- Dwie zmienne obiektowe, obie bazujace na numerach (np. 1-4 dla koloru, 2-14 dla wartosci karty, bo As jest najwyzszy). Zaleta jest to ze latwiej bedzie napisac algorytm sortowania pewnie. Potrzebna metoda klasowa, ktora rozszyfruje jaka jest nazwa koloru i jaka nazwa karty.
- Dwie zmienne, z czego obie wskazuja na obiekty odpowiednio: KolorKarty i WartoscKarty. Te obiekty z kolei maja po jednej zmiennej, chyba z wartoscia liiczbowa, ktora da sie rozszyfrowac. Sam nie wiem, to na dobra sprawe jest to samo rozwiazanie chyba.

- LookupTable w metodzie klasowej, przy inicjalizacji i jako zmienna klasowa
- metoda klasowa do konwersji z numerkow, osobna dla kolorow i wartosci, osobna dla krotki i dlugich nazw


Moze to sie daje (albo powinno) jakos z symbolami rozwiazac?

'!
!Karta categoriesForClass!Kernel-Objects! !
!Karta methodsFor!

color
	^color!

color: c value: v
	color := c.
	value := v!

displayOn: aStream
	"aStream nextPutAll: (self class displayColor: color) , ' ' , (self class displayValue: value)"

	aStream nextPutAll: (self class displayValue: value)!

printOn: target
	target nextPutAll: (self class printValue: value) , ' ' , (self class printColor: color)!

value
	^value! !
!Karta categoriesFor: #color!public! !
!Karta categoriesFor: #color:value:!public! !
!Karta categoriesFor: #displayOn:!public! !
!Karta categoriesFor: #printOn:!public! !
!Karta categoriesFor: #value!public! !

!Karta class methodsFor!

color: c value: v
	^super new color: c value: v!

displayColor: colorIndex
	^#('T' 'K' 'C' 'P') at: colorIndex!

displaySortAlgorithm
	^QuicksortAlgorithm new setSortBlock: [:a :b | a color = b color ifTrue: [a value >= b value] ifFalse: [a color > b color]]!

displayValue: valueIndex
	valueIndex < 11 ifTrue: [^valueIndex displayString].
	^#('W' 'D' 'K' 'A') at: valueIndex - 10

	"
	valueIndex < 10 ifTrue: [^valueIndex displayString].
	^#('T' 'W' 'D' 'K' 'A') at: valueIndex - 9
	"!

printColor: colorIndex
	| colorNames |
	colorNames := #('trefl' 'karo' 'kier' 'pik').
	^colorNames at: colorIndex!

printSortAlgorithm
	^QuicksortAlgorithm new setSortBlock: [:a :b | a color = b color ifTrue: [a value <= b value] ifFalse: [a color < b color]]!

printValue: valueIndex
	valueIndex < 11 ifTrue: [^valueIndex printString].
	^#('Walet' 'Dama' 'Krol' 'As') at: valueIndex - 10! !
!Karta class categoriesFor: #color:value:!public! !
!Karta class categoriesFor: #displayColor:!public! !
!Karta class categoriesFor: #displaySortAlgorithm!public! !
!Karta class categoriesFor: #displayValue:!public! !
!Karta class categoriesFor: #printColor:!public! !
!Karta class categoriesFor: #printSortAlgorithm!public! !
!Karta class categoriesFor: #printValue:!public! !

Rozdanie guid: (GUID fromString: '{CCCC265D-92FF-4C65-B9CB-C70A27508CCB}')!
Rozdanie comment: ''!
!Rozdanie categoriesForClass!Kernel-Objects! !
!Rozdanie methodsFor!

displayOn: aStream
	players allButLast do: 
			[:p |
			p displayOn: aStream.
			aStream cr].
	players last displayOn: aStream!

initialize
	| undealt |
	undealt := self class deck.
	players := #('N' 'E' 'S' 'W') collect: [:n | Gracz name: n cards: (self takeRandom13: undealt)]!

printOn: target
	players allButLast do: 
			[:p |
			p printOn: target.
			target cr].
	players last printOn: target!

takeRandom13: cards
	| r toTake result |
	cards size < 13 ifTrue: [^self error: 'Not enough cards left in the deck!!'].
	result := OrderedCollection new.
	r := Random new.
	toTake := 13.
	[toTake > 0] whileTrue: 
			[| cardIndex |
			cardIndex := (r next * (cards size - 1)) rounded + 1.
			result add: (cards removeAtIndex: cardIndex).
			toTake := toTake - 1].
	^result! !
!Rozdanie categoriesFor: #displayOn:!public! !
!Rozdanie categoriesFor: #initialize!private! !
!Rozdanie categoriesFor: #printOn:!public! !
!Rozdanie categoriesFor: #takeRandom13:!private! !

!Rozdanie class methodsFor!

deck
	deckSingleton isNil ifFalse: [^deckSingleton copy].
	deckSingleton := OrderedCollection new.
	(1 to: 4) do: [:c | (2 to: 14) do: [:v | deckSingleton add: (Karta new color: c value: v)]].
	^deckSingleton copy!

new
	^super new initialize! !
!Rozdanie class categoriesFor: #deck!private! !
!Rozdanie class categoriesFor: #new!public! !

"Binary Globals"!

