| package |
package := Package name: 'Mimuw1'.
package paxVersion: 1;
	basicComment: ''.


package classNames
	add: #A1;
	add: #A10;
	add: #A2;
	add: #A3;
	add: #A4;
	add: #A5;
	add: #A6;
	add: #A7;
	add: #A8;
	add: #A9;
	add: #Osoba;
	add: #Wyrozumiala;
	yourself.

package binaryGlobalNames: (Set new
	yourself).

package globalAliases: (Set new
	yourself).

package setPrerequisites: (IdentitySet new
	add: 'Core\Object Arts\Dolphin\Base\Dolphin';
	yourself).

package!

"Class Definitions"!

Object subclass: #A1
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #Osoba
	instanceVariableNames: 'imie nazwisko'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #Wyrozumiala
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
A1 subclass: #A2
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
A2 subclass: #A3
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
A3 subclass: #A4
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
A4 subclass: #A5
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
A5 subclass: #A6
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
A6 subclass: #A7
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
A7 subclass: #A8
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
A8 subclass: #A9
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
A9 subclass: #A10
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!

"Global Aliases"!


"Loose Methods"!

"End of package definition"!

"Source Globals"!

"Classes"!

A1 guid: (GUID fromString: '{21B19D76-6841-4175-921D-79D00DF11010}')!
A1 comment: ''!
!A1 categoriesForClass!Unclassified! !
!A1 methodsFor!

m ^1! !
!A1 categoriesFor: #m!public! !

Osoba guid: (GUID fromString: '{AB5D9866-4401-4F9A-AA78-F9EB2D26411D}')!
Osoba comment: ''!
!Osoba categoriesForClass!Kernel-Objects! !
!Osoba methodsFor!

imie
	^imie!

imie: a nazwisko: b
	imie := a.
	nazwisko := b!

nazwisko
	^nazwisko!

printOn: aStream
	imie , ' ' , nazwisko printOn: aStream! !
!Osoba categoriesFor: #imie!public! !
!Osoba categoriesFor: #imie:nazwisko:!public! !
!Osoba categoriesFor: #nazwisko!public! !
!Osoba categoriesFor: #printOn:!public! !

!Osoba class methodsFor!

imie: a nazwisko: b
	^super new imie: a nazwisko: b! !
!Osoba class categoriesFor: #imie:nazwisko:!public! !

Wyrozumiala guid: (GUID fromString: '{452A890E-B735-47BC-9F8B-41DF7F12A733}')!
Wyrozumiala comment: ''!
!Wyrozumiala categoriesForClass!Kernel-Objects! !
!Wyrozumiala methodsFor!

doesNotUnderstand: m
	self class methodDictionary keysDo: 
		[:k | 
		((k podobneDo: m selector zTolerancja: 2) and: [k argumentCount = m argumentCount])
			ifTrue: [^self perform: k withArguments: m arguments]].
	^super doesNotUnderstand: m!

proba
	^'Dziala !!'! !
!Wyrozumiala categoriesFor: #doesNotUnderstand:!public! !
!Wyrozumiala categoriesFor: #proba!public! !

A2 guid: (GUID fromString: '{65A054D4-1D27-4E7B-9572-6349563E5BA5}')!
A2 comment: ''!
!A2 categoriesForClass!Unclassified! !
!A2 methodsFor!

m ^2 * super m! !
!A2 categoriesFor: #m!public! !

A3 guid: (GUID fromString: '{E440FECA-CFBB-42A4-AAD5-412633C2A196}')!
A3 comment: ''!
!A3 categoriesForClass!Unclassified! !
!A3 methodsFor!

m ^2 * super m! !
!A3 categoriesFor: #m!public! !

A4 guid: (GUID fromString: '{EED4D1D8-923D-44FF-91FF-DB6A277E088C}')!
A4 comment: ''!
!A4 categoriesForClass!Unclassified! !
!A4 methodsFor!

m ^2 * super m! !
!A4 categoriesFor: #m!public! !

A5 guid: (GUID fromString: '{ADA79281-1F7C-4D33-B5A1-83B5A8D85FCE}')!
A5 comment: ''!
!A5 categoriesForClass!Unclassified! !
!A5 methodsFor!

m ^2 * super m! !
!A5 categoriesFor: #m!public! !

A6 guid: (GUID fromString: '{02714FCB-C485-445F-A9C9-BD237E8137D0}')!
A6 comment: ''!
!A6 categoriesForClass!Unclassified! !
!A6 methodsFor!

m ^2 * super m! !
!A6 categoriesFor: #m!public! !

A7 guid: (GUID fromString: '{B6364A01-7E41-4F6D-9790-BB4FB2BC64E8}')!
A7 comment: ''!
!A7 categoriesForClass!Unclassified! !
!A7 methodsFor!

m ^2 * super m! !
!A7 categoriesFor: #m!public! !

A8 guid: (GUID fromString: '{16F13A77-6EF4-46DD-8740-F8BA6A3B9A1B}')!
A8 comment: ''!
!A8 categoriesForClass!Unclassified! !
!A8 methodsFor!

m ^2 * super m! !
!A8 categoriesFor: #m!public! !

A9 guid: (GUID fromString: '{F6EBDF99-6896-4E76-8E5D-2D776DF64E3E}')!
A9 comment: ''!
!A9 categoriesForClass!Unclassified! !
!A9 methodsFor!

m ^2 * super m! !
!A9 categoriesFor: #m!public! !

A10 guid: (GUID fromString: '{20B729F5-F959-4E90-9D7F-0AE1C41744EE}')!
A10 comment: ''!
!A10 categoriesForClass!Unclassified! !
!A10 methodsFor!

m ^2 * super m! !
!A10 categoriesFor: #m!public! !

"Binary Globals"!

