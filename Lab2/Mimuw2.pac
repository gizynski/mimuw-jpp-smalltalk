| package |
package := Package name: 'Mimuw2'.
package paxVersion: 1;
	basicComment: ''.


package classNames
	add: #Kolejka2Stos;
	add: #StosElement;
	add: #StosKolekcja;
	add: #StosLista;
	add: #TesterPierwszosci;
	yourself.

package binaryGlobalNames: (Set new
	yourself).

package globalAliases: (Set new
	yourself).

package setPrerequisites: (IdentitySet new
	add: 'Core\Object Arts\Dolphin\Base\Dolphin';
	yourself).

package!

"Class Definitions"!

Object subclass: #Kolejka2Stos
	instanceVariableNames: 'waiting ready'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #StosElement
	instanceVariableNames: 'previous value'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #StosKolekcja
	instanceVariableNames: 'lista'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #StosLista
	instanceVariableNames: 'top'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: ''!
Object subclass: #TesterPierwszosci
	instanceVariableNames: 'knownPrimes'
	classVariableNames: ''
	poolDictionaries: ''
	classInstanceVariableNames: 'singleton'!

"Global Aliases"!


"Loose Methods"!

"End of package definition"!

"Source Globals"!

"Classes"!

Kolejka2Stos guid: (GUID fromString: '{8D49373D-51C8-446E-9148-DF4070B6F439}')!
Kolejka2Stos comment: ''!
!Kolejka2Stos categoriesForClass!Kernel-Objects! !
!Kolejka2Stos methodsFor!

initialize
	waiting := StosLista new.
	ready := StosLista new!

insert: element
	waiting push: element!

isEmpty
	^ready isEmpty & waiting isEmpty!

remove
	self isEmpty ifTrue: [^self error: 'Nie mozna zrobic remove na pustej kolejce'].
	ready isEmpty ifFalse: [^ready pop].
	[waiting isEmpty] whileFalse: [ready push: waiting pop].
	^ ready pop.! !
!Kolejka2Stos categoriesFor: #initialize!public! !
!Kolejka2Stos categoriesFor: #insert:!public! !
!Kolejka2Stos categoriesFor: #isEmpty!public! !
!Kolejka2Stos categoriesFor: #remove!public! !

!Kolejka2Stos class methodsFor!

new
	^super new initialize! !
!Kolejka2Stos class categoriesFor: #new!public! !

StosElement guid: (GUID fromString: '{B13EB573-04A0-49BB-9862-27D4B87575B2}')!
StosElement comment: ''!
!StosElement categoriesForClass!Kernel-Objects! !
!StosElement methodsFor!

previous
	^previous!

previous: aStosElement
	previous := aStosElement!

printElementOn: aStream
	aStream nextPutAll: value printString.
	previous == nil
		ifFalse: 
			[aStream nextPutAll: ' '.
			previous printElementOn: aStream]!

value
	^value!

value: anObject
	value := anObject! !
!StosElement categoriesFor: #previous!public! !
!StosElement categoriesFor: #previous:!public! !
!StosElement categoriesFor: #printElementOn:!public! !
!StosElement categoriesFor: #value!public! !
!StosElement categoriesFor: #value:!public! !

!StosElement class methodsFor!

value: anObject previous: aStosElement
	^(super new)
		value: anObject;
		previous: aStosElement! !
!StosElement class categoriesFor: #value:previous:!public! !

StosKolekcja guid: (GUID fromString: '{7E72D6F1-DFBF-43B9-9951-9C1BC8392DE3}')!
StosKolekcja comment: 'Klasa Stosu zrobiona w ramach treningu z laboratorium Smalltalk 2.
Implementacja jest oparta o wbudowana kolekcje OrderedCollection, wiec nie jest to implementacja taka jak zadana.

Zrobilem ja, bo wydawala mi sie prostsza i to wygladalo jak dobry kandydat na trening przed podejsciem do faktycznego zadania.

Niby bylo to niepotrzebne, ale nauczylem sie robienia class instance method ''initialize'' i zrobilem ladne wypisywanie stosu.'!
!StosKolekcja categoriesForClass!Kernel-Objects! !
!StosKolekcja methodsFor!

initialize
	lista := OrderedCollection new.!

isEmpty
	^lista isEmpty.!

pop
	self isEmpty
		ifTrue: [^self error: 'Stos jest pusty, nie mozna zrobic pop']
		ifFalse: [^lista removeFirst]!

printOn: aStream
	| name |
	name := self class name.
	aStream
		nextPutAll: (name first isVowel ifTrue: ['an '] ifFalse: ['a ']);
		nextPutAll: name;
		nextPutAll: '( '.
	lista do: [:element | aStream nextPutAll: element printString , ' '].
	aStream nextPutAll: ')'.!

push: element
	lista addFirst: element.
	^ element.! !
!StosKolekcja categoriesFor: #initialize!private! !
!StosKolekcja categoriesFor: #isEmpty!public! !
!StosKolekcja categoriesFor: #pop!public! !
!StosKolekcja categoriesFor: #printOn:!public! !
!StosKolekcja categoriesFor: #push:!public! !

!StosKolekcja class methodsFor!

new
	^super new initialize.! !
!StosKolekcja class categoriesFor: #new!public! !

StosLista guid: (GUID fromString: '{9B010C00-3393-49C6-A4E3-15C5D38FDB7E}')!
StosLista comment: ''!
!StosLista categoriesForClass!Kernel-Objects! !
!StosLista methodsFor!

isEmpty
	^top == nil!

pop
	self isEmpty
		ifTrue: [self error: 'Nie mozna zrobic pop na pustym stosie']
		ifFalse: 
			[| value |
			value := top value.
			top := top previous.
			^value]!

printOn: aStream
	| name |
	name := self class name.
	aStream
		nextPutAll: (name first isVowel ifTrue: ['an '] ifFalse: ['a ']);
		nextPutAll: name;
		nextPutAll: '('.
	self isEmpty
		ifFalse: [top printElementOn: aStream].
	aStream nextPutAll: ')'.!

push: aValue
	top := StosElement value: aValue previous: top! !
!StosLista categoriesFor: #isEmpty!public! !
!StosLista categoriesFor: #pop!public! !
!StosLista categoriesFor: #printOn:!public! !
!StosLista categoriesFor: #push:!public! !

TesterPierwszosci guid: (GUID fromString: '{5F8281A3-BDAA-4787-B920-ED110E6ACD3E}')!
TesterPierwszosci comment: ''!
!TesterPierwszosci categoriesForClass!Kernel-Objects! !
!TesterPierwszosci methodsFor!

initialize
	knownPrimes := OrderedCollection new.
	knownPrimes addLast: 2.!

jestPierwsza: anInteger
	" czy tutaj powinna byc weryfikacja ze przekazany obiekt jest klasy na ktorej mozna wykonywac operacje arytmetyczne itp? "

	| limit smallerKnownPrimes |
	limit := anInteger sqrt.
	smallerKnownPrimes := knownPrimes select: [:x | x <= limit].
	smallerKnownPrimes do: [:x | anInteger \\ x = 0 ifTrue: [^false]].

	(knownPrimes last >= limit) ifTrue: [^true].
	(knownPrimes last + 1 to: limit) do: 
			[:x |
			(self jestPierwsza: x)
				ifTrue: 
					[knownPrimes addLast: x.
					anInteger \\ x = 0 ifTrue: [^false]]].
	^true.! !
!TesterPierwszosci categoriesFor: #initialize!private! !
!TesterPierwszosci categoriesFor: #jestPierwsza:!public! !

!TesterPierwszosci class methodsFor!

default
	" czy tutaj chodzilo o to zeby zrobic to metoda klasowa initialize albo cos w tym stylu? Bo zrobilem takie leniwe inizjalizowanie "
	singleton == nil ifTrue: [^singleton := TesterPierwszosci new] ifFalse: [^singleton]!

new
	^super new initialize!

test
	| numberOfTests r max actualPrimeNumbersInRange testNumbers tester |
	tester := self default.
	max := 5000.
	actualPrimeNumbersInRange := Integer primesUpTo: max.
	numberOfTests := 50.
	r := Random new.
	" pewnie jest jakis ladniejszy sposob na stworzenie takiej kolekcji, jakies sugestie? "
	(1 to: numberOfTests) do: 
			[:i |
			| x |
			x := (r next * max) rounded.
			(tester jestPierwsza: x) = (actualPrimeNumbersInRange includes: x)
				ifTrue: 
					[Transcript
						show: 'Test ' , i printString , '/' , numberOfTests printString , ': liczba ' , x printString
									, ' prawidlowo przeanalizowana, tzn. jestPierwsza dalo ' , (tester jestPierwsza: x) printString;
						cr]
				ifFalse: [^self error: 'Liczba ' , x printString , ' zle przeanalizowana']].
	^true! !
!TesterPierwszosci class categoriesFor: #default!public! !
!TesterPierwszosci class categoriesFor: #new!public! !
!TesterPierwszosci class categoriesFor: #test!public! !

"Binary Globals"!

