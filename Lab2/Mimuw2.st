" ---------------------------------------------------------------------------------------- "
" Zadanie 1 - Stos "
" Pierwsze podejscie, implementacja oparta na kolekcji OrderedCollection "

s1 := StosKolekcja new.
s1 push: 1 ; push: 2 ; push: 3.
s1 push: 4.
s1 pop.
s1 isEmpty.
s1.

" ---------------------------------------------------------------------------------------- "
" Zadanie 1 - Stos "
" Drugie podejscie, implementacja listowa (dwie klasy) "

s2 := StosLista new.
s2 isEmpty.
s2 push: 4.
s2 push: 1 ; push: 2 ; push: 3.
s2 pop.
s2 printString.

" ---------------------------------------------------------------------------------------- "
" Zadanie 2 - Kolejka "
" Kolejka ma byc zrealizowana za pomoca dwoch stosow, wiec bede uzywal StosLista "

k := Kolejka2Stos new.
k isEmpty.
k insert: 1 ; insert: 2 ; insert: 3 ; insert: 4 ; insert: 5.
k remove.
k insert: 6 ; insert: 7 ; insert: 8.
k.

" ---------------------------------------------------------------------------------------- "
" Zadanie 3 - Tester Pierwszosci "
" Opis na Moodle, ma byc metoda instancji #jestPierwsza i klasowe #default (chodzi o singleton), #test "

t := TesterPierwszosci new.
t jestPierwsza: 89.
t jestPierwsza: 4452.
t jestPierwsza: 1097.

TesterPierwszosci test.
TesterPierwszosci default.

Integer primesUpTo: 9000.

" ---------------------------------------------------------------------------------------- "
" Robol "

c := OrderedCollection new.
c isEmpty
c addLast: 5.
c removeLast
c

o := OrderedCollection new add: 7 ; 
o do: [:each | Transcript show: each printString].

Integer primesUpTo: 80.

$A class.
3 + 2 ; + 1 ; * 2.

Date today inspect.

ProfStef go.

krzysiek := Osoba imie: 'Krzys' nazwisko: 'Ciekawski'.

(10 @ 100) class classPoint class

5 cubed

'abc' reverse.
'abc' isPalindrome.
'aba' isPalindrome.

5 between: 6 and: 3.
5 smallerThan: 4 orGreaterThan: 2




